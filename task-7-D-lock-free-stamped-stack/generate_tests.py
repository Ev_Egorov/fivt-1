#!/usr/bin/env python3

def output_test(test_num, test_data):
    with open("{test_num:02d}.in".format(test_num=test_num), "w") as f:
        print(test_data, file=f)
    with open("{test_num:02d}.in.out".format(test_num=test_num), "w") as f:
        pass

def generate_tests():
    for threads, batch, inserts in [
        (100000, 1, 1),
        (100000, 2, 2),
        (100000, 5, 5),
        (1000000, 1, 20),
        (1000000, 2, 20),
        (1000000, 5, 20),
        (1000000, 10, 20),
        (1000000, 30, 20),
        (1000000, 50, 20),
        (1000000, 100, 20),
    ]:
        yield "{threads} {batch} {inserts}".format(**locals())

def main():
    for test_num, test_data in enumerate(generate_tests()):
        output_test(test_num, test_data)

main()
