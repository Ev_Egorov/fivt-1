#include "affinity.h"
#include "asserts.h"
#include "barrier.h"
#include "executor.h"
#include "flaky_cas_atomic.h"
#include "program_options.h"

//#include "lock_free_stack.h"
#include "solution.h"

#include <atomic>
#include <random>
#include <string>
#include <sstream>
#include <vector>
#include <thread>

//////////////////////////////////////////////////////////////////////

struct TestElement {
    TestElement(const size_t thread_index, const size_t index)
        : thread_index_(thread_index)
        , index_(index)
        , payload_(std::to_string(index)) {
    }

    TestElement() {
    }

    size_t thread_index_{0};
    size_t index_{0};
    std::string payload_{};
};

//////////////////////////////////////////////////////////////////////

class ConcurrentStackTester {
public:
    ConcurrentStackTester(const size_t num_inserts, const size_t batch_size, const size_t num_threads)
        : num_inserts_{num_inserts},
          batch_size_{batch_size},
          num_threads_{num_threads},
          stack_{/* capacity = */ batch_size * num_threads},
          start_barrier_{num_threads} {
    }

    void operator ()() {
        RunTest();
    }

private:
    void RunTest() {
        RunThreads();
        ValidateFinalInvariants();
    }

    void RunThreads() {
        TaskExecutor executor;
        for (size_t t = 0; t < num_threads_; ++t) {
            executor.Execute([this, t]() {
                RunContenderThread(t);
            });
        }
    }

    void RunContenderThread(const size_t thread_index) {
        size_t consumed_sum = 0;
        std::vector<size_t> prev_indices(num_threads_, -1);

        std::mt19937 random_generator;
        std::uniform_int_distribution<> next_int(1, batch_size_);

        start_barrier_.Pass();

        size_t i = thread_index;
        while (i < num_inserts_) {
            const size_t batch_size = next_int(random_generator);

            size_t inserted = 0;
            size_t j = 0;

            while (j < batch_size && i < num_inserts_) {
                ++inserted;

                TestElement element(thread_index, i);
                stack_.Push(element);

                ++j;
                i += num_threads_;
            }

            for (size_t k = 0; k < inserted; ++k) {
                TestElement element{};
                test_assert(stack_.Pop(element), "non-empty stack expected");
                ValidatePoppedElement(element);
                consumed_sum += element.index_;
            }
        }

        total_consumed_sum_.fetch_add(consumed_sum);
    }

    void ValidatePoppedElement(const TestElement& element) const {
        test_assert(element.thread_index_ < num_threads_, "invalid element's thread index");
        test_assert(element.index_ % num_threads_ == element.thread_index_, "inconsistent element's index and thread index");
    }

    void ValidateFinalInvariants() {
        TestElement element{};
        test_assert(!stack_.Pop(element), "empty stack expected");

        const size_t expected_sum = num_inserts_ * (num_inserts_ - 1) / 2;
        test_assert(total_consumed_sum_.load() == expected_sum, "unexpected total consumed sum");
    }

private:
    size_t num_inserts_;
    size_t batch_size_;
    size_t num_threads_;

    template <typename T> using Atomic = FlakyCasAtomic<T, 10>;
    LockFreeStack<TestElement, Atomic> stack_;

    OnePassBarrier start_barrier_;

    std::atomic<size_t> total_consumed_sum_{0};
};

///////////////////////////////////////////////////////////////////////

void RunTest(int argc, char* argv[]) {
    size_t num_inserts;
    size_t batch_size;
    size_t num_threads;

    read_opts(argc, argv, num_inserts, batch_size, num_threads);

    // use in local test to increase chances of A-B-A
    // AttachThreadsToSingleCore();

    ConcurrentStackTester{num_inserts, batch_size, num_threads}();
}

int main(int argc, char* argv[]) {
    RunTest(argc, argv);
    return EXIT_SUCCESS;
}
