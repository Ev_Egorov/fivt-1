#pragma once

#include <atomic>
#include <new>
#include <type_traits>

//////////////////////////////////////////////////////////////////////

using ArenaRef = uint32_t;

const ArenaRef ARENA_NULL_REF = 0u;

template <typename Object>
class Arena {
    using ObjectStorage = typename std::aligned_storage<sizeof(Object), alignof(Object)>::type;
public:
    Arena(const uint32_t capacity)
        : arena_(new ObjectStorage[capacity + 1]) {
        // reserve zero offset for null reference
        AllocateObjectStorage();
    }

    ~Arena() {
        DestroyAllocatedObjects();
        ReleaseMemory();
    }

    template <typename ...Args>
    ArenaRef New(Args&&... args) {
        const uint32_t offset = AllocateObjectStorage();
        void* address = arena_ + offset;
        new (address) Object(std::forward<Args>(args)...);
        return offset;
    }

    Object* ToPointer(ArenaRef ref) const {
        if (ref == ARENA_NULL_REF) {
            return nullptr;
        }
        return reinterpret_cast<Object*>(arena_ + ref);
    }

private:
    void DestroyAllocatedObjects() {
        const uint32_t tail_offset = next_offset_.load();
        for (uint32_t offset = 1; offset < tail_offset; ++offset) {
            Object* object = ToPointer(offset);
            object->~Object();
        }
    }

    void ReleaseMemory() {
        delete[] arena_;
    }

    uint32_t AllocateObjectStorage() {
        return next_offset_.fetch_add(1);
    }

private:
    ObjectStorage* arena_;
    std::atomic<uint32_t> next_offset_{0};
};


using Stamp = uint32_t;

struct alignas(8) ArenaStampedRef {
    ArenaRef ref_;
    Stamp stamp_;

    ArenaStampedRef(ArenaRef ref = ARENA_NULL_REF, Stamp stamp = 0) noexcept
        : ref_(ref),
          stamp_(stamp) {
    }

    bool operator ==(const ArenaStampedRef& that) const {
        return ref_ == that.ref_ &&
               stamp_ == that.stamp_;
    }

    bool operator !=(const ArenaStampedRef& that) const {
        return !(*this == that);
    }

    operator bool() const {
        return ref_ != ARENA_NULL_REF;
    }

    ArenaStampedRef RedirectTo(ArenaRef new_ref) const {
        // create new reference with advanced version number (stamp)
        return ArenaStampedRef(new_ref, stamp_ + 1);
    }
};

//////////////////////////////////////////////////////////////////////
