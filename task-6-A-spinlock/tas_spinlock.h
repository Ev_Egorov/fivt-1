#pragma once

#include <atomic>
#include <mutex>

/////////////////////////////////////////////////////////////////////

// Test-And-Set spinlock
class TASSpinLock {
public:
    void Lock() {
        while (locked_.exchange(true /*, memory order */)) {
            std::this_thread::yield();
        }
    }

    void Unlock() {
        locked_.store(false /*, memory order */);
    }

private:
    std::atomic<bool> locked_{false};
};

/////////////////////////////////////////////////////////////////////
