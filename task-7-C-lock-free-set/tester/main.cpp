#include "arena_allocator.h"
#include "asserts.h"
#include "barrier.h"
#include "executor.h"
#include "program_options.h"
//#include "ya_contest_sim.h"

#include "solution.h"
//#include "optimistic_linked_set.h"
//#include "lock_free_linked_set.h"

#include <algorithm>
#include <random>
#include <thread>

///////////////////////////////////////////////////////////////////////

class ConcurrentSetTester {
public:
    explicit ConcurrentSetTester(const size_t num_inserts, const size_t num_threads, const size_t num_iterations)
        : allocator_(EstimateArenaSize(num_inserts, num_threads, num_iterations)),
          set_(allocator_),
          num_inserts_{num_inserts},
          num_threads_{num_threads},
          num_iterations_{num_iterations},
          barrier_{num_threads} {
    }

    void operator ()() {
        RunContenderThreads();
    }

private:
    void RunContenderThreads() {
        TaskExecutor executor{};
        for (size_t thread_index = 0; thread_index < num_threads_; ++thread_index) {
            executor.Execute([this, thread_index]() {
                RunContenderThread(thread_index);
            });
        }
    }

    void RunContenderThread(const size_t thread_index) {
        for (size_t i = 0; i < num_iterations_; ++i) {
            RunTestIteration(thread_index);
        }
    }

    void RunTestIteration(const size_t thread_index) {
        barrier_.Pass();

        // phase 1
        // iinsert all even numbers
        // concurrent inserts, disjoint working sets

        for (int e = thread_index; e < (int)num_inserts_; e += num_threads_) {
            if (e % 2 == 0) {
                test_assert(set_.Insert(e), "[phase 1] element already in set: " << e);
                test_assert(set_.Contains(e), "[phase 1] expected element not found: " << e);
                if (e % 5 == 0) {
                    test_assert(!set_.Insert(e), "[phase 1] duplicated insert: " << e);
                }
            } else {
                test_assert(!set_.Contains(e), "[phase 1] unexpected element found: " << e);
            }
        }

        barrier_.Pass();

        test_assert(set_.Size() == num_inserts_ / 2, "unexpected set size: " << set_.Size());

        barrier_.Pass();

        // phase 2
        // remove all even numbers and insert all odd numbers
        // concurrent inserts and removes

        for (int e = 0; e < (int)num_inserts_; ++e) {
            if (e % 2 == 0) {
                set_.Remove(e);
                test_assert(!set_.Contains(e), "[phase 2] unexpected element found: " << e);
                if (e % (thread_index + 1) == 0) {
                    test_assert(!set_.Remove(e), "[phase 2] duplicated remove: " << e);
                }
            } else {
                set_.Insert(e);
            }
        }

        barrier_.Pass();

        // phase 3
        // remove all odd elements
        // concurrent removes

        for (int e = 0; e < (int)num_inserts_; ++e) {
            if (e % 2 == 0) {
                test_assert(!set_.Contains(e), "[phase 3] unexpected element found: " << e);
            } else {
                set_.Remove(e);
                test_assert(!set_.Contains(e), "[phase 3] unexpected element found: " << e);
            }
        }

        barrier_.Pass();

        test_assert(set_.Size() == 0, "unexpected set size");
    }

    static size_t EstimateArenaSize(size_t num_inserts, size_t num_threads, size_t num_iterations) {
        return num_iterations * num_inserts * num_threads * 32 /* expected node size in bytes */ * 2 /* reserve factor */;
    }

private:
    ArenaAllocator allocator_;
    ConcurrentSet<int> set_;

    size_t num_inserts_;
    size_t num_threads_;
    size_t num_iterations_;

    Barrier barrier_;
};

///////////////////////////////////////////////////////////////////////

void RunTest(int argc, char* argv[]) {
    size_t num_inserts;
    size_t num_threads;
    size_t num_iterations;

    read_opts(argc, argv, num_inserts, num_threads, num_iterations);

    //SolutionTests::SimulateYandexContest();

    ConcurrentSetTester{num_inserts, num_threads, num_iterations}();
}

int main(int argc, char* argv[]) {
    RunTest(argc, argv);
    return EXIT_SUCCESS;
}
