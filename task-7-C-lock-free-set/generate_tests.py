#!/usr/bin/env python3

def output_test(test_num, test_data):
    with open("{test_num:02d}.in".format(test_num=test_num), "w") as f:
        print(test_data, file=f)

def generate_tests():
    for threads, inserts, iters in [
        (1000, 1, 1),
        (10, 2, 10000),
        (10, 5, 5000),
        (10, 10, 5000),
        (10, 20, 2000),
        (100, 2, 1000),
        (100, 5, 500),
        (100, 10, 200),
        (100, 20, 100),
        (1000, 2, 10),
        (1000, 5, 5),
        (1000, 10, 5),
        (1000, 20, 1),
        (3000, 20, 1),
    ]:
        yield "{threads} {inserts} {iters}".format(**locals())

def main():
    for test_num, test_data in enumerate(generate_tests()):
        output_test(test_num, test_data)

main()
