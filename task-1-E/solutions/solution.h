#pragma once

#include <cstddef>

class TreeMutex {
public:
    TreeMutex(std::size_t /*num_threads*/) {}

    void lock(std::size_t /*current_thread*/) {}

    void unlock(std::size_t /*current_thread*/) {}
};

