#!/usr/bin/env python3

def output_test(test_num, test_data):
    with open("{test_num:02d}.in".format(test_num=test_num), "w") as f:
        print(test_data, file=f)

def generate_tests():
    for threads, iters, shareds in [
        (1,  100000, 1),
        (2,  100000, 1),
        (2,  100000, 2),
        (2,  100000, 5),
        (2,  100000, 10),
        (5,  50000, 1),
        (5,  50000, 2),
        (5,  50000, 5),
        (5,  25000, 10),
        (10,  20000, 1),
        (10,  10000, 2),
        (10,  10000, 5),
        (10,  5000, 10),
        (20,  5000, 1),
        (20,  3000, 2),
        (20,  2000, 5),
        (20,  1000, 10),
    ]:
        yield "{threads} {iters} {shareds}".format(**locals())

def main():
    for test_num, test_data in enumerate(generate_tests()):
        output_test(test_num, test_data)

main()
